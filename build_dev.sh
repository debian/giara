#!/bin/sh

pwd
rm -rf build
mkdir build
cd build
meson ..
meson configure -Dprefix=$PWD/build/testdir -Dprofile="development"
ninja
ninja install
