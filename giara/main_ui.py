from giara.constants import RESOURCE_PREFIX
from gi.repository import Gtk, Adw, GLib
from giara.confManager import ConfManager
from giara.main_deck import MainDeck


class MainUI(Adw.Bin):
    def __init__(self):
        super().__init__(vexpand=True, hexpand=True)
        self.confman = ConfManager()
        self.reddit = self.confman.reddit
        self.builder = Gtk.Builder.new_from_resource(
            f'{RESOURCE_PREFIX}/ui/main_ui.ui'
        )
        self.ui_box = self.builder.get_object('ui_box')
        self.notif_revealer = self.builder.get_object('revealer')
        self.notif_label = self.builder.get_object('notif_label')
        self.notif_close_btn = self.builder.get_object('notif_close_btn')
        self.notif_close_btn.connect('clicked', self.hide_notif)

        self.deck = MainDeck()
        self.ui_box.append(self.deck)

        self.set_child(self.builder.get_object('overlay1'))
        self.show()

    def show_notification(self, text=None):
        if text is not None:
            self.notif_label.set_text(text)
        self.notif_revealer.set_reveal_child(True)
        GLib.timeout_add_seconds(5, self.hide_notif)

    def hide_notif(self, *args):
        self.notif_revealer.set_reveal_child(False)
