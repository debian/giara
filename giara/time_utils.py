from gi.repository import GLib
from datetime import datetime
from dateutil import tz


def utc_datetime_to_now(in_time: datetime) -> datetime:
    return in_time.astimezone(tz.tzlocal())


def utc_timestamp_to_now(timestamp: str) -> datetime:
    return utc_datetime_to_now(
        datetime.fromtimestamp(timestamp, tz.tzutc())
    )


def humanize(dt: datetime) -> str:
    tzl = tz.tzlocal()
    timezone_seconds = tzl.utcoffset(dt).seconds
    timezone_str = '{0}{1}:{2}'.format(
        '+' if timezone_seconds >= 0 else '',
        format(int(timezone_seconds/3600), '02'),
        format(int(
            (timezone_seconds - (int(timezone_seconds/3600)*3600))/60
        ), '02')
    )
    return GLib.DateTime(
        GLib.TimeZone(timezone_str),
        dt.year, dt.month, dt.day, dt.hour, dt.minute, dt.second
    ).to_local().format('%-e %b %Y\n%X')


def humanize_utc_timestamp(timestamp: str) -> str:
    return humanize(utc_timestamp_to_now(timestamp))
