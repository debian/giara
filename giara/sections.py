from gi.repository import Gtk
from giara.post_preview_listview import PostPreviewListView
from giara.markdown_view import MarkdownView
from giara.subreddits_list_view import SubredditsListbox
from giara.giara_clamp import new_clamp
from giara.scrolled_win import GiaraScrolledWin


class SectionScrolledWindow(GiaraScrolledWin):
    def __init__(self, post_preview_lbox):
        super().__init__()
        self.post_preview_lbox = post_preview_lbox
        self.connect('edge_reached', self.on_edge_reached)

    def on_edge_reached(self, sw, pos):
        if pos == Gtk.PositionType.BOTTOM:
            self.post_preview_lbox.load_more()


class PostListSection(SectionScrolledWindow):
    def __init__(self, gen_func, show_post_func, load_now=False, source=None):
        self.gen_func = gen_func
        self.listview = PostPreviewListView(
            self.gen_func,
            show_post_func,
            source,
            load_now=load_now
        )
        super().__init__(self.listview)
        self.set_child(self.listview)
        self.refresh = self.listview.refresh


class MarkdownSection(GiaraScrolledWin):
    def __init__(self, markdown):
        self.clamp = new_clamp()
        self.markdown = markdown
        self.markdown_view = MarkdownView(self.markdown)
        self.markdown_view.get_style_context().add_class('card')
        self.markdown_view.get_style_context().add_class('padding12')
        self.clamp.set_child(self.markdown_view)
        super().__init__()
        self.set_child(self.clamp)


class SubredditsSection(GiaraScrolledWin):
    def __init__(self, generator, show_subreddit_func, load_now=False):
        self.clamp = new_clamp()
        self.generator = generator
        self.show_subreddit_func = show_subreddit_func
        self.subreddit_listbox = SubredditsListbox(
            self.generator,
            self.show_subreddit_func,
            load_now=load_now
        )
        self.clamp.set_child(self.subreddit_listbox)
        super().__init__()
        self.set_child(self.clamp)
        self.refresh = self.subreddit_listbox.refresh
