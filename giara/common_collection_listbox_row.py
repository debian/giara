from gi.repository import Gtk, Pango
from giara.simple_avatar import SimpleAvatar


class CCLBRLabel(Gtk.Label):
    def __init__(self, **kwargs):
        super().__init__(
            wrap=True, wrap_mode=Pango.WrapMode.WORD_CHAR, vexpand=True,
            hexpand=True, halign=Gtk.Align.START,
            justify=Gtk.Justification.FILL, margin_start=12,
            **kwargs
        )


class CommonCollectionListboxRow(Gtk.ListBoxRow):
    def __init__(self, name, title, get_icon_func, avatar_name=None, **kwargs):
        super().__init__(**kwargs)
        self.main_box = Gtk.Box(
            orientation=Gtk.Orientation.HORIZONTAL,
            margin_start=12, margin_end=12, margin_top=12, margin_bottom=12
        )

        self.name = name
        self.title = title
        self.get_icon_func = get_icon_func

        self.avatar = SimpleAvatar(
            42,
            avatar_name or title,
            self.get_icon_func
        )

        self.label = CCLBRLabel(
            label=(title or name)
        )
        self.desc_label = CCLBRLabel(label=name)
        self.labels_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)

        for lbl in (self.label, self.desc_label):
            self.labels_box.append(lbl)

        self.desc_label.get_style_context().add_class('subtitle')

        self.main_box.append(self.avatar)
        self.main_box.append(self.labels_box)
        self.set_child(self.main_box)

    def get_key(self):
        return self.name + ' ' + self.title
