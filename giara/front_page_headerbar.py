from giara.constants import RESOURCE_PREFIX
from gettext import gettext as _, ngettext
from gi.repository import Gtk, Gio
from giara.new_post_window import NewPostWindow
from giara.download_manager import download_img
from giara.squeezing_viewswitcher_headerbar import \
    SqueezingViewSwitcherHeaderbar
from giara.simple_avatar import SimpleAvatar


class FrontPageHeaderbar(SqueezingViewSwitcherHeaderbar):
    def __init__(self, front_page_section, **kwargs):
        super().__init__(
            Gtk.Builder.new_from_resource(
                f'{RESOURCE_PREFIX}/ui/headerbar.ui'
            ),
            front_page_section.listview,
            view_switcher=False,
            sort_menu=True,
            sorting_methods={
                'best': {
                    'name': _('Best'),
                    'icon': 'best-symbolic'
                },
                'hot': {
                    'name': _('Hot'),
                    'icon': 'hot-symbolic'
                },
                'new': {
                    'name': _('New'),
                    'icon': 'new-symbolic'
                },
                'top': {
                    'name': _('Top'),
                    'icon': 'arrow1-up-symbolic'
                },
                'rising': {
                    'name': _('Rising'),
                    'icon': 'rising-symbolic'
                },
                'controversial': {
                    'name': _('Controversial'),
                    'icon': 'controversial-symbolic'
                }
            },
            default_method='from_conf',
            **kwargs
        )

        self.menu_btn = self.builder.get_object(
            'menu_btn'
        )
        self.menu_popover = self.builder.get_object('menu_popover')

        self.new_post_action_group = Gio.SimpleActionGroup()
        for a in ('link', 'text', 'media'):
            c_action = Gio.SimpleAction.new(a, None)
            c_action.connect(
                'activate', self.on_new_clicked
            )
            self.new_post_action_group.add_action(c_action)
        self.insert_action_group('newpost', self.new_post_action_group)

        self.new_btn = self.builder.get_object('new_btn')

        self.profile_btn = self.builder.get_object('profile_btn')
        self.username_label = self.builder.get_object('username_label')
        self.karma_label = self.builder.get_object('karma_label')
        self.user = self.reddit.user.me()
        self.username_label.set_text(f'u/{self.user.name}')
        self.karma_label.set_text(ngettext(
            '{0} Karma', '{0} Karma', self.user.total_karma
        ).format(self.user.total_karma))
        self.avatar_container = self.builder.get_object('avatar_container')
        self.avatar = SimpleAvatar(
            42, self.user.name, lambda *args: download_img(self.user.icon_img)
        )
        self.avatar_container.append(self.avatar)

        self.inbox_badge = self.builder.get_object('inbox_count_badge_label')
        self.confman.connect(
            'notif_count_change',
            self.on_inbox_count_change
        )

        self.refresh_btn = self.builder.get_object('refresh_btn')
        self.search_btn = self.builder.get_object('search_btn')

    def on_inbox_count_change(self, caller, count):
        if int(count) <= 0:
            self.inbox_badge.set_text('')
            self.inbox_badge.set_visible(False)
        else:
            self.inbox_badge.set_text(count)
            self.inbox_badge.set_visible(True)
            self.inbox_badge.show()

    def on_menu_btn_clicked(self, *args):
        self.menu_popover.popup()

    def on_new_clicked(self, action, param):
        np_win = NewPostWindow(self.reddit, action.get_name())
        np_win.set_transient_for(self.get_root())
        np_win.present()
