from gi.repository import Gtk, Pango


def make_ellipsized_label(txt: str) -> Gtk.Label:
    label = Gtk.Label.new(txt)
    label.set_ellipsize(Pango.EllipsizeMode.END)
    return label
