desktop_conf = configuration_data()
desktop_conf.set('bindir', join_paths(prefix, bindir))
desktop_conf.set('prettyname', prettyname)
# .desktop comment now hardcoded for better i18n support
#desktop_conf.set('description', description)
if profile == 'development'
    desktop_conf.set('appid', app_id+'.dev')
else
    desktop_conf.set('appid', app_id)
endif
desktop_conf.set('projectname', meson.project_name())

desktop_file = configure_file(
    input: app_id + '.desktop.in',
    output: app_id + '.desktop.i18n.in',
    configuration: desktop_conf
)

i18n.merge_file(
    input: desktop_file,
    output: app_id + '.desktop',
    po_dir: '../po',
    type: 'desktop',
    install: true,
    install_dir: join_paths(datadir, 'applications')
)

dbus_conf = configuration_data()
dbus_conf.set('bindir', join_paths(prefix, bindir))
dbus_conf.set('appid', app_id)
dbus_conf.set('projectname', meson.project_name())

configure_file(
    input: app_id + '.service.in',
    output: app_id + '.service',
    configuration: dbus_conf,
    install_dir: join_paths(datadir, 'dbus-1/services')
)

gschema_conf = configuration_data()
gschema_conf.set('apppath', app_id_aspath)
gschema_conf.set('appid', app_id)
gschema_conf.set('projectname', meson.project_name())

configure_file(
    input: app_id + '.gschema.xml.in',
    output: app_id + '.gschema.xml',
    configuration: gschema_conf,
    install_dir: join_paths(datadir, 'glib-2.0/schemas')
)

icondir = join_paths(datadir, 'icons/hicolor')
if profile == 'development'
    install_data(
        'icons/'+app_id+'.dev.svg',
        install_dir: join_paths(icondir, 'scalable/apps')
    )
else
    install_data(
        'icons/'+app_id+'.svg',
        install_dir: join_paths(icondir, 'scalable/apps')
    )
endif
install_data(
    'icons/'+app_id+'-symbolic.svg',
    install_dir: join_paths(icondir, 'symbolic/apps')
)

subdir('ui')

service_desktop_conf = configuration_data()
service_desktop_conf.set('prettyname', prettyname)
service_desktop_conf.set('projname', meson.project_name())
service_desktop_conf.set('app_id', app_id)

# gresource_conf = configuration_data()
# gresource_conf.set('app_id_in_aspath', app_id_in_aspath)
# gresource_conf.set('app_id_aspath', app_id_aspath)
# gresource_conf.set('app_id', app_id)
# gresource_conf.set('app_id_in', app_id_in)

blueprints = custom_target('blueprints',
    input: files(
        'ui/comment_box.blp',
        'ui/headerbar.blp',
        'ui/choice_picker_popover_content.blp',
        'ui/login_view.blp',
        'ui/headerbar_with_back_and_squeezer.blp',
        'ui/inbox_item_view.blp',
        'ui/main_ui.blp',
        'ui/post_body.blp',
        'ui/post_details_headerbar.blp',
        'ui/shortcutsWindow.blp',
        'ui/image_viewer_window.blp',
        'ui/redditor_heading.blp',
        'ui/subreddit_heading.blp',
        'ui/subreddit_view_headerbar.blp',
        # 'ui/new_post_window.blp',  # TODO: needs GtkFileFilter
    ),
    output: '.',
    command: [find_program('blueprint-compiler'), 'batch-compile', '@OUTPUT@', '@CURRENT_SOURCE_DIR@', '@INPUT@']
)

app_resources = gnome.compile_resources(
    app_id,
    app_id + '.gresource.xml',
    # configure_file(  # alt config
    #     input: app_id_in + '.gresource.xml.in',
    #     output: app_id + '.gresource.xml',
    #     configuration: gresource_conf
    # ),
    gresource_bundle: true,
    dependencies: [
        blueprints,
        configure_file(
            input: 'ui/aboutdialog.ui.in',
            output: 'aboutdialog.ui',
            configuration: glade_conf
        ),
        configure_file(
            input: app_id + '.service.desktop.in',
            output: app_id + '.service.desktop',
            configuration: service_desktop_conf
        )
    ],
    install: true,
    install_dir: pkgdatadir
)

app_settings = gnome.compile_schemas()

#appdata_conf = configuration_data()
#appdata_conf.set('authorfullname', authorfullname)
#appdata_conf.set('gitrepo', gitrepo)
#appdata_conf.set('website', website)
#appdata_conf.set('authoremail', authoremail)
#appdata_conf.set('prettyname', prettyname)
#appdata_conf.set('appid', app_id)
#appdata_conf.set('prettylicense', prettylicense)
#
#configure_file(
#    input: appdata_file,
#    output: app_id + '.appdata.xml.',
#    configuration: appdata_conf,
#    install: true,
#    install_dir: join_paths(datadir, 'metainfo')
#)

ascli_exe = find_program('appstreamcli', required: false)
if ascli_exe.found()
    test(
        'validate metainfo file',
        ascli_exe,
        args: [
            'validate',
            #'--no-net',
            #'--pedantic',
            'data/' + app_id + '.appdata.xml'
        ]
    )
endif

i18n.merge_file(
    input: app_id + '.appdata.xml.in',
    output: app_id + '.appdata.xml',
    po_dir: '../po',
    install: true,
    install_dir: join_paths(datadir, 'metainfo')
)
